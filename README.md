# format-flowed.vim

Dynamically set VIM's formatoptions and tab settings by the location of the cursor in a mail buffer.

This is intended to allow for the ease the editing of headers, signatures,
quoted text, and patches while composing
[format=flowed](http://joeclark.org/ffaq.html) plaintext mail.

## Installation

Add the repository's Git URL
(https://gitlab.com/lafrenierejm/vim-format-flowed.git) to your favorite plugin
manager's list of plugins.

Using [vim-plug](https://github.com/junegunn/vim-plug), this would be
```
Plug 'https://gitlab.com/lafrenierejm/vim-format-flowed.git'
```

## Recognized sections within a mail buffer

The following rules are applied on top of mail filetype settings specified
outside of this script (e.g., in ftplugin/mail.vim).

### Header

Header blocks are paragraphs whose first line starts with the string "From:".

Formatting:
* `-a` - do not automatically reformat paragraphs
* `-t` - do not auto-wrap paragraphs at `textwidth`
* `-w` - trailing whitespace does not effect paragraph continuation

Tabs:
* `noexpandtab` - do not replace <TAB> characters with equivalent spaces
* `shiftwidth=8` - each indent level is 8 columns
* `softtabstop=0` - disable `softtabstop`

### Signature

Signature blocks start with a (possibly quoted) line containing only the string
"-- " (note the single trailing space).

Formatting:
* `-a` - do not automatically reformat paragraphs
* `-t` - do not auto-wrap paragraphs at `textwidth`
* `-w` - trailing whitespace does not effect paragraph continuation

Tabs:
* `noexpandtab` - do not replace <TAB> with spaces
* `shiftwidth=8` - each indent level is 8 columns
* `softtabstop=0` - disable `softtabstop`

### Quotation

Each line of a quote block starts with '>'.

Formatting:
* `+a` - automatically reformat paragraphs as they are typed
* `+t` - auto-wrap paragraphs at `textwidth`
* `+w` - trailing whitespace does not effect paragraph continuation

Tabs:
* `expandtab` - replace <TAB> with an equivalent number of spaces

### Patch

Block composed of a version control patch.

Formatting:
* `-a` - do not automatically reformat paragraphs
* `-t` - do not auto-wrap paragraphs at `textwidth`
* `-w` - trailing whitespace does not effect paragraph continuation

Tabs:
* `noexpandtab` - do not replace <TAB> with spaces
* `shiftwidth=8` - each indent level is 8 columns
* `softtabstop=0` - disable `softtabstop`

### Normal body text

Text that does not fit into any of the above categories.

Formatting:
* `+a` - automatically reformat paragraphs as they are typed
* `+t` - auto-wrap paragraphs at `textwidth`
* `+w` - trailing whitespace does not effect paragraph continuation

Tabs:
* `expandtab` - replace <TAB> with an equivalent number of spaces

## License

Copyright © Joseph LaFreniere <joseph@lafreniere.xyz>.  Distributed under the
permissive [ISC
License](https://gitlab.com/lafrenierejm/vim-format-flowed/blob/64ce76bb87a5ec788fba1b4d31efc9b6d83233cf/LICENSE).

## Credit

The plugin is based on a [script posted by Teemu
Likonen](http://groups.google.com/group/vim_use/msg/f59e5c1adc6be2b3) in the
vim_user Google Group.
