" Dynamically set format options depending on where you are in a mail.
"
" Written by Joseph LaFreniere (lafrenierejm) <joseph@lafreniere.xyz>
"
" Based on a script by Teemu Likonen:
" http://groups.google.com/group/vim_use/msg/f59e5c1adc6be2b3

let s:default_fo='setlocal formatoptions='.&formatoptions

function! s:MailAreaDetect_On()
	silent autocmd! MailAreaDetect CursorMoved,CursorMovedI
				\ <buffer> call <SID>AreaOptions()
endfunction

augroup MailAreaDetect
	autocmd!
	call <SID>MailAreaDetect_On()
augroup END

function! s:AreaOptions()
	execute s:default_fo
	if <SID>CheckArea('\v^From( |: ).*\n','\v^$')
		"echo 'Header'
		setlocal formatoptions-=a " do not automatically reformat paragraphs
		setlocal formatoptions-=t " do not auto-wrap at textwidth
		setlocal formatoptions-=w " trailing whitespace does not effect paragraph
		setlocal noexpandtab      " do not replace <TAB> with spaces
		setlocal shiftwidth=8     " number of columns for each indent
		setlocal softtabstop=0    " disable softtabstop
	elseif <SID>CheckArea('^>* \=-- $','^$')
		"echo 'Signature'
		setlocal formatoptions-=a " do not automatically reformat paragraphs
		setlocal formatoptions-=t " do not auto-wrap at textwidth
		setlocal formatoptions-=w " trailing whitespace does not effect paragraph
		setlocal noexpandtab      " do not replace <TAB> with spaces
		setlocal shiftwidth=8     " number of columns for each indent
		setlocal softtabstop=0    " disable softtabstop
	elseif getline('.')=~#'^>.*'
		"echo 'Quotation'
		setlocal formatoptions+=a " automatically reformat paragraphs
		setlocal formatoptions+=t " auto-wrap paragraphs at textwidth
		setlocal formatoptions+=w " trailing whitespace continues paragraph
	elseif <SID>CheckArea('\m^--- .*\n^+++ ','\v(^$|\n^-- $)')
		"echo 'Patch'
		setlocal formatoptions-=a " do not automatically reformat paragraphs
		setlocal formatoptions-=t " do not auto-wrap at textwidth
		setlocal formatoptions-=w " trailing whitespace does not effect paragraph
		setlocal noexpandtab      " do not replace <TAB> with spaces
		setlocal shiftwidth=8     " number of columns for each indent
		setlocal softtabstop=0    " disable softtabstop
	else
		"echo 'My text'
		setlocal formatoptions+=a " automatically reformat paragraphs
		setlocal formatoptions+=t " auto-wrap paragraphs at textwidth
		setlocal formatoptions+=w " trailing whitespace continues paragraph
		setlocal expandtab        " replace <TAB> with equivalent spaces
	endif
endfunction

function! s:CheckArea(start, end)
	return (search(a:start,'bcnW')-line('.')) >
				\ (search(a:end,'bnW')-line('.'))
endfunction
